package mx.edu.ittepic.practicau3_3_trivia_barajasvaldiviaagustin;

import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by agustin on 30/03/18.
 */

public class ConexionWeb extends AsyncTask<URL,String, String>{
    MainActivity puntero;

    public ConexionWeb(MainActivity p){
        puntero = p;
    }

    @Override
    protected String doInBackground(URL... urls) {

        //Generar cadena de envío
        String respuesta="";

        //Conexión con el servidor
        HttpURLConnection conexion = null;

        try{
            publishProgress("Generando nueva pregunta");
            conexion = (HttpURLConnection)urls[0].openConnection();
            conexion.setDoInput(true);
            conexion.setRequestMethod("POST");
            conexion.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            OutputStream flujoSalida = new BufferedOutputStream(conexion.getOutputStream());
            flujoSalida.flush();
            flujoSalida.close();

            if (conexion.getResponseCode() == 200) {
                InputStreamReader input = new InputStreamReader(conexion.getInputStream(), "UTF-8");
                BufferedReader flujoEntrada = new BufferedReader(input);
                String linea = "";
                do{
                    linea = flujoEntrada.readLine();
                    if (linea!=null){
                        respuesta += linea;
                    }
                }while(linea != null);
                flujoEntrada.close();
            }else
                return "ERROR_404_1";

        }catch (UnknownHostException e){
            return "ERROR_404_2";
        }catch (IOException er) {
            //NO se pueden enviar o recibir datos
            return "ERROR_404_1";
        }finally {
            if (conexion != null){
                conexion.disconnect();
            }
        }
        return respuesta;
    }

    protected void onProgressUpdate(String... r){
        puntero.cambiarMensaje(r[0]);
    }

    protected void onPostExecute(String respuesta){
        puntero.procesarRespuesta(respuesta);
    }
}
