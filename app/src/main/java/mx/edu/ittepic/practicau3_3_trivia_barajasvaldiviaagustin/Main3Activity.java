package mx.edu.ittepic.practicau3_3_trivia_barajasvaldiviaagustin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    TextView acercade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        setTitle("");
        String datos = "Trivia\n" +
                "Instituto Tecnológico de Tepic\n" +
                "Materia: Taller de programación de dispositivos móviles\n" +
                "Alumno: Barajas Valdivia Agustin\n" +
                "Docente: MTI. Castillo Valtierra Sergio Benigno\n" +
                "Versión 1.0";
        acercade = findViewById(R.id.acercade);

        acercade.setText(datos);
    }
}
