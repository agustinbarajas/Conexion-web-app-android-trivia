package mx.edu.ittepic.practicau3_3_trivia_barajasvaldiviaagustin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView correctos, incorrectos, adivinanza, opciones;
    Button opcion1, opcion2, opcion3, nuevapregunta;
    ImageView vida1, vida2, vida3;
    int correct, incorrect, partida;
    ConexionWeb conexionweb;
    ProgressDialog dialog;
    String solucion;
    String[] options;
    List<Integer> numpregunta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        correctos = findViewById(R.id.correctos);
        incorrectos = findViewById(R.id.incorrectos);
        adivinanza = findViewById(R.id.adivinanza);
        opciones = findViewById(R.id.opciones);
        opcion1 = findViewById(R.id.opca);
        opcion2 = findViewById(R.id.opcb);
        opcion3 = findViewById(R.id.opcc);
        nuevapregunta = findViewById(R.id.nueva);
        vida1 = findViewById(R.id.vida1);
        vida2 = findViewById(R.id.vida2);
        vida3 = findViewById(R.id.vida3);
        solucion = "";
        numpregunta = new ArrayList<>();
        deshabilitarBotones();
        inicio();
        correct = incorrect = partida = 0;

        nuevapregunta.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                inicio();
            }
        });
        opcion1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                evaluar(0);
            }
        });
        opcion2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                evaluar(1);
            }
        });
        opcion3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                evaluar(2);
            }
        });
    }
    private void evaluar(int pos){
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        ImageView image = new ImageView(this);
        if (options[pos].equals(solucion)){
            correct++;
            correctos.setText(correct+"");
            //mostrar imágen de esfera correcta
            image.setImageResource(R.drawable.esfera);
            alert.setTitle("Correcto");
        }else{
            incorrect++;
            incorrectos.setText(incorrect+"");
            if (incorrect == 1) {
                vida3.setImageResource(R.drawable.blanco);
                //mostrar imágen de esfera incorrecta nivel 1
                image.setImageResource(R.drawable.esferafragmentada);
                alert.setTitle("Incorrecto: primer error");
            }else if(incorrect == 2){
                vida2.setImageResource(R.drawable.blanco);
                //mostrar imagen de esfera incorrecta nivel 2
                image.setImageResource(R.drawable.esferapartescaidas);
                alert.setTitle("Incorrecto: segundo error");
            }else if(incorrect == 3){
                vida1.setImageResource(R.drawable.blanco);
                //mostrar imágen de esfera incorrecta nivel 3
                image.setImageResource(R.drawable.esferaenpedazos);
                alert.setTitle("Incorrecto: tercer error")
                .setMessage("Fin del juego\nUsted ha perdido");
            }
        }
        if (correct >= 7 && (correct+incorrect) == 10){
            alert.setMessage("Fin del juego\nUsted ha ganado");
        }
        alert.setView(image)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
        .show();
        deshabilitarBotones();
        nuevapregunta.setEnabled(true);
    }
    private void inicio(){
        try {
            if ((correct+incorrect) >= 10 || incorrect >= 3) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                if (correct >= 7){
                    alert.setTitle("Felicidades!")
                            .setMessage("Fin del juego\nUsted ha ganado");
                }else {
                    alert.setTitle("ALERTA")
                        .setMessage("Juego terminado\n¿Iniciar nuevo juego?");
                }
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        nuevoJuego();
                        dialog.dismiss();
                    }
                })
                .show();
                return;
            }
            conexionweb = new ConexionWeb(MainActivity.this);
            URL direccion = new URL("https://tpdmagustin.000webhostapp.com/trivia.php");
            dialog = ProgressDialog.show(MainActivity.this, "Espere", "Generando nueva pregunta");
            conexionweb.execute(direccion);
            nuevapregunta.setEnabled(false);
        }catch(MalformedURLException e){
            Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_LONG).show();
        }
    }
    private void deshabilitarBotones(){
        opcion1.setEnabled(false);
        opcion2.setEnabled(false);
        opcion3.setEnabled(false);
    }
    public void procesarRespuesta(String respuesta) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("ADVERTENCIA");
        dialog.dismiss();
        if (respuesta.equals("ERROR_404_1")){
            respuesta = "ERROR: Flujo de Entrada/Salida no funcionó";
            alert.setMessage(respuesta).show();
            nuevapregunta.setEnabled(true);
            return;
        }
        if (respuesta.equals("ERROR_404_2")){
            respuesta = "ERROR: Servidor caido o dirección incorrecta";
            alert.setMessage(respuesta).show();
            nuevapregunta.setEnabled(true);
            return;
        }
        int num = Integer.parseInt(respuesta.split("&")[1]);
        if (numpregunta.contains(num)){
            inicio();
            return;
        }
        numpregunta.add(num);
        respuesta = respuesta.split("&")[0];
        String[] partes = respuesta.split("#");
        options = partes[1].split(":");
        solucion = options[3];
        adivinanza.setText(partes[0]);
        opciones.setText("a) "+options[0]+"\nb) "+options[1]+"\nc) "+options[2]);
        opcion1.setEnabled(true);
        opcion2.setEnabled(true);
        opcion3.setEnabled(true);
    }
    public void cambiarMensaje(String s) {
        dialog.setMessage(s);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem mi){
        switch (mi.getItemId()){
            case R.id.nuevo:
                nuevoJuego();
                break;
            case R.id.salir:
                finish();
                break;
            case R.id.about:
                Intent acercade = new Intent(MainActivity.this, Main3Activity.class);
                startActivity(acercade);
                break;
        }
        return true;
    }
    private void nuevoJuego(){//Iniciar nueva partida
        vida1.setImageResource(R.drawable.vida);
        vida2.setImageResource(R.drawable.vida);
        vida3.setImageResource(R.drawable.vida);
        correctos.setText(0 + "");
        incorrectos.setText(0 + "");
        adivinanza.setText("");
        opciones.setText("");
        correct = 0;
        incorrect = 0;
        partida = 0;
        numpregunta.clear();
        deshabilitarBotones();
        nuevapregunta.setEnabled(true);
        inicio();
    }
}
