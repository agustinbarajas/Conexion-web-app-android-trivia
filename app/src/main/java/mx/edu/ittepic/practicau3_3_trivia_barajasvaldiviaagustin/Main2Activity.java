package mx.edu.ittepic.practicau3_3_trivia_barajasvaldiviaagustin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    Button start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        start = findViewById(R.id.start);
        setTitle("");

        start.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent newActivity = new Intent(Main2Activity.this, MainActivity.class);
                startActivity(newActivity);
                finish();
            }
        });
    }
}
